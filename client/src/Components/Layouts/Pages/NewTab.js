import React, {useEffect } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { MDBRow, MDBCol } from 'mdbreact';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';



function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [category, setCategory] = React.useState('');
  const [optionData, setOptionData] = React.useState('');
  const [mix, setMix] = React.useState('');
  const [description, setDescription] = React.useState('');
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
      const mystyle = {
      color: "DodgerBlue",
      textAlign:'center',
      padding: "10px",
      marginTop:"100px",
      fontFamily: "Arial"
    };
    const setValues = (value) => {
      let defaultValues = mix.find(item => item.category === value);
      if (!!defaultValues) {
          const { body = '' } = defaultValues;
          setDescription(body);
      }else{
          setDescription('');
      }
   }
    useEffect(() => {
        fetch('https://us-central1-reacttab.cloudfunctions.net/app/api/read', {
              method: 'GET',
            }).then(res => res.json())
              .then((res) => {
                console.log("----res----",res);
                if (!!res) {
                let data = res;
                setMix(data);      
                let categorydata = [];
                for (var i = data.length - 1; i >= 0; i--) {
                  categorydata.push(data[i].category);
                }
                let optionItems_category = categorydata.map((data) =>
                        <MenuItem value={data}>{data}</MenuItem>
                    );
                setOptionData(optionItems_category);
            }
          });
    }, []);
  return (

    <div className={classes.root}>
    <h3 style={mystyle}>This is react tab example.</h3>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} centered aria-label="simple tabs example">
          <Tab label="Joke" {...a11yProps(0)} />
          <Tab label="Item Two" {...a11yProps(1)} />
          <Tab label="Item Three" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
                    <MDBRow style={{ marginTop: '2%' }}>
                        <MDBCol md={2}>
                            <FormControl style={{ width: '100%', textAlign: 'left' }}>
                                <InputLabel style={{ width: '100%', textAlign: 'left' }}>Category</InputLabel>
                                <Select
                                    style={{ width: '100%', textAlign: 'left' }}
                                    value={category}
                                    onChange={(e) => {
                                        let category = e.target.value;
                                        setCategory(category);
                                        setValues(category);

                                    }}
                                    inputProps={{
                                        name: 'Category',
                                        id: 'Category',
                                    }}
                                >
                                    {optionData}
                                </Select>
                            </FormControl>
                        </MDBCol>
                    </MDBRow>
                    <MDBRow style={{ marginTop: '2%' }}>
                        <MDBCol md={8}>
                            <FormControl style={{ width: '100%', textAlign: 'left' }}>
                                {description}
                            </FormControl>
                        </MDBCol>
                    </MDBRow>
      </TabPanel>
      <TabPanel value={value} index={1}>
        Item Two
      </TabPanel>
      <TabPanel value={value} index={2}>
        Item Three
      </TabPanel>
    </div>
  );
}