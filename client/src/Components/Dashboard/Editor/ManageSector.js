import React, { useState, useEffect } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBIcon, MDBInput, MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import './editor.css';
import { Modal } from 'antd';
import Upload from './Upload';

const { confirm } = Modal;

const ManageSector = () => {

    const [sector, setSector] = useState('');
    const [title, setTitle] = useState('');
    const [manageSector, setManageSector] = useState('');
    const [id, setId] = useState('');
    const [editsector, setEditSector] = useState('');
    const [edittitle, setEditTitle] = useState('');
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        fetchManageSectorData();
    }, []);
    const submitHandler = (e) => {
        e.preventDefault();
        let data = new FormData();
        data.append('title', title);
        data.append('sector', sector);
        fetch('/managesector', {
            method: 'POST',
            body: data
        })
            .then(res => {
                if (res.status === 200) {
                    fetchManageSectorData()
                }
            })
            .catch(error => {
                console.log('Please check your connection');
            })
    }

    const fetchManageSectorData = () => {
        setSector('');
        setTitle('');
        fetch(`/managesector`)
            .then(res => res.json())
            .then(res => {
                setManageSector(res.result)
            })
            .catch(error => {
                console.log('Please check your internet connection..!');
            })
    }
    const edit = (id) => {
        let editableArrary = manageSector.filter(item => item._id === id);
        const [editable = {}] = editableArrary;
        const { _id = '', title = '', sector = '' } = editable;
        setId(_id);
        setEditSector(sector);
        setEditTitle(title);
        setVisible(!visible);
    }
    const handleEdit = (e) => {
        e.preventDefault()
        let data = new FormData();
        data.append('title', edittitle);
        data.append('sector', editsector);
        fetch(`/managesector/${id}`, {
            method: 'PATCH',
            body: data,
        }).then(res => {
            if (res.status === 201) {
                setId('');
                setEditSector('');
                setEditTitle('');
                setVisible(!visible);
                fetchManageSectorData();
            }
        })
    }

    const deleteHandler = (id) => {
        fetch(`/managesector/${id}`, {
            method: 'DELETE',
            headers: { 'content-type': 'application/json' }
        })
            .then(res => {
                if (res.status === 200) {
                    fetchManageSectorData();
                }
            })
    }
    const showDeleteConfirm = (id) => {
        confirm({
            title: 'Do you Want to delete this item?',
            onOk() {
                deleteHandler(id);
            },
            onCancel() {
                console.log('Cancel', id);
            },
        });
    }


    return (
        <MDBContainer className="text-center">
            <p className="h4 text-center mb-4">Sector Management</p>
            <MDBRow>
                <MDBCol md="6">
                    <form onSubmit={submitHandler} encType='multipart/form-data'>

                        <MDBRow style={{ marginTop: '2%' }}>
                            <MDBCol md={12}>
                                <FormControl style={{ width: '100%', textAlign: 'left' }}>
                                    <InputLabel htmlFor="Sector" style={{ width: '100%', textAlign: 'left' }}>Sector</InputLabel>
                                    <Select
                                        style={{ width: '100%', textAlign: 'left' }}
                                        value={sector}
                                        onChange={(e) => {
                                            setSector(e.target.value);
                                        }}
                                        inputProps={{
                                            name: 'Sector',
                                            id: 'Sector',
                                        }}
                                    >
                                        <MenuItem value='Category'>Category</MenuItem>
                                        <MenuItem value='Subcategory'>Subcategory</MenuItem>
                                        <MenuItem value='Brand'>Brand</MenuItem>
                                        <MenuItem value='Brandmodel'>Brand model</MenuItem>
                                    </Select>
                                </FormControl>
                            </MDBCol>
                        </MDBRow>
                        <MDBRow>
                            <MDBCol md="12">
                                <MDBInput
                                    label="Name"
                                    type="text"
                                    value={title}
                                    onChange={(e) => {
                                        setTitle(e.target.value);
                                    }}
                                />
                            </MDBCol>
                        </MDBRow>
                        <div className="text-center mt-4">
                            <MDBBtn color="info" outline type="submit">
                                Save <MDBIcon far icon="paper-plane" className="ml-2" />
                            </MDBBtn>
                        </div>
                    </form>
                </MDBCol>
                <MDBCol md="6">
                    <MDBTable style={{ marginTop: '25px', }}>
                        <MDBTableHead >
                            <tr>
                                
                                <th>Name</th>
                                <th>Level</th>
                                <th>Actions</th>
                            </tr>
                        </MDBTableHead>
                        <MDBTableBody>
                            {
                                manageSector.length ?
                                    manageSector.map(item => {
                                        return (
                                            <tr key={item._id}>

                                                <td>{item.title}</td>
                                                <td>{item.sector}</td>
                                                <td>
                                                    <DeleteForeverIcon onClick={() => showDeleteConfirm(item._id)} />
                                                    <EditIcon onClick={() => edit(item._id)} />
                                                </td>
                                            </tr>
                                        )
                                    })
                                    : null
                            }
                        </MDBTableBody>
                    </MDBTable>
                </MDBCol>
            </MDBRow>
            <Modal
                visible={visible}
                title="Edit Sector"
                onOk={handleEdit}
                onCancel={() => { setVisible(!visible) }}
            >
                <form encType='multipart/form-data'>
                    <MDBRow>
                        <MDBCol md="12">
                            <MDBInput
                                label="Name"
                                type="text"
                                value={edittitle}
                                onChange={(e) => {
                                    let title = e.target.value;
                                    setEditTitle(title);
                                }}
                            />
                        </MDBCol>
                    </MDBRow>


                    <MDBRow style={{ marginTop: '2%' }}>
                        <MDBCol md={12}>
                            <FormControl style={{ width: '100%', textAlign: 'left' }}>
                                <InputLabel htmlFor="Sector" style={{ width: '100%', textAlign: 'left' }}>Sector</InputLabel>
                                <Select
                                    style={{ width: '100%', textAlign: 'left' }}
                                    value={editsector}
                                    onChange={(e) => {
                                        let editsector = e.target.value;
                                        setEditSector(editsector);

                                    }}
                                    inputProps={{
                                        name: 'Sector',
                                        id: 'Sector',
                                    }}
                                >
                                    <MenuItem value='Category'>Category</MenuItem>
                                    <MenuItem value='Subcategory'>Subcategory</MenuItem>
                                    <MenuItem value='Brand'>Brand</MenuItem>
                                </Select>
                            </FormControl>
                        </MDBCol>
                    </MDBRow>
                    
                </form>
            </Modal>
        </MDBContainer>
    );
};

export default ManageSector;